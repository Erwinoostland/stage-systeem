@extends('layouts.app')

@section('content')
	
	<section class="row">
		<section class="col-md-12 mt-3">

			{{-- Session messages --}}
			@if(\Session::has('success'))
				<p class="alert alert-success card">{{ \Session::get('success') }}</p>
			@elseif (\Session::has('error'))
				<p class="alert alert-danger card">{{ \Session::get('error') }}</p>
			@endif
			{{-- End session messages --}}

			<section class="row">
				@if(empty($searchResults[0]))
					<h3 class="mt-100">Geen resultaten gevonden...</h3>
				@endif
					@foreach($searchResults->where('id', '!=', \Auth::id()) as $searchResult)
						<section class="col-md-6">
							<section class="card">
								<section class="card-body">
									<section class="row">
									{{-- Profile Picture --}}
									<section class="col-md-3">
										<section class="profile--picture">
											
										</section>
									</section>

									{{-- Information --}}
									<section class="col-md-8 mt-2">
										<section class="searchResult--info">
											<table class="borderless">
												<tr>
													<td>Naam:</td>
													@if (!empty($searchResult->firstname))
														<th>{{ $searchResult->firstname . " " . $searchResult->lastname }}</th>
													@else
														<th>Naam is niet bekend...</th>
													@endif
												</tr>
												<tr>
													<td>Email:</td>
													@if (!empty($searchResult->email))
														<th>{{ $searchResult->email }}</th>
													@else
														<th>Email is niet bekend...</th>
													@endif								
												</tr>
												<tr>
													<td>Geboortedatum:</td>
													@if (!empty($searchResult->dateOfBirth))
														<th>{{ date('d-m-Y', strtotime($searchResult->dateOfBirth)) }}</th>
													@else
														<th>Geboortedatum is niet bekend...</th>
													@endif
												</tr>
											</table>
											<br />
											<i class="float-right">{{ $searchResult->checkRelationship() }}</i>

											</p>
										</section>
									</section>

									<section class="col-md-1">
										@if ($searchResult->friended() == true)
											<a data-toggle="modal" data-target="#addFriend{{$searchResult->id}}">
												<i class="material-icons tableIcon float-right">
													add_box
												</i>
											</a>
										@endif
										<a href="" class="a-no-decoration">
											<i class="material-icons tableIcon float-right">
												remove_red_eye
											</i>
										</a>

										{{-- Add FriendRequest Modal --}}
										<section class="modal fade" id="addFriend{{$searchResult->id}}" tabindex="-1" role="dialog" aria-labelledby="addFriendLabel{{$searchResult->id}}" aria-hidden="true">
										  <section class="modal-dialog" role="document">
										    <section class="modal-content">
										      <section class="modal-header">
										        <h5 class="modal-title" id="addFriendLabel{{$searchResult->id}}">Vriend toevoegen</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </section>
										      <form method="post" action="{{ url('/search/sendrequest/'.$searchResult->id) }}">
										      	{{ csrf_field() }}
											      <section class="modal-body">
											      	@if (!empty($searchResult->firstname) && !empty($searchResult->lastname))
										        		<p>
										        			Wil je <b>{{ $searchResult->firstname . " " . $searchResult->lastname }}</b> een vriendschapverzoek sturen?
										        		</p>
										        	@else
										        		<p>
										        			Wil je deze persoon toevoegen als vriend?
										        		</p>
										        	@endif
											      </section>
											      <section class="modal-footer">
											        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
											        <button type="submit" class="btn btn-primary red--button">Verstuur</button>
											      </section>
										      </form>
										    </section>
										  </section>
										</section>
										{{-- End add FriendRequest modal --}}

									</section>
								</section>
							</section>
						</section>
					</section>
				@endforeach
			</section>
		</section>
	</section>
@endsection