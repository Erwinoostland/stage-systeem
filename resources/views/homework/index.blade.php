@extends('layouts.app')

@section('content')
	<section class="row">
		<section class="col-md-12">
			<section class="card">

				{{-- Card Header --}}
				<section class="card-header">
					<h3>Huiswerk
						<a href="" data-toggle="modal" data-target="#addHomeworkModal" class="red--icon">
							<span class="float-right">
								<i class="material-icons">add</i>
							</span>
						</a>
					</h3>
				</section>

				{{-- Card Body --}}
				<section class="card-body">

					{{-- Session Flash Messages --}}
					@if(Session::has('success'))
						<p id="sessionMessage" class="alert alert-success">{{ Session::get('success') }}</p>
					@endif

					{{-- Session Flash Messages --}}
					@if(Session::has('danger'))
						<p id="sessionMessage" class="alert alert-danger">{{ Session::get('danger') }}</p>
					@endif

					{{-- Homework Table --}}
					<section class="table-responsive">
						<table class="table table-striped">
							@foreach($lessons as $lesson)
								<thead>
									<tr>
										<th colspan="4" class="thColor">
											<b>{{ $lesson->name }} <a onclick="showTable({{$lesson->id}})"><i class="material-icons tableIcon">arrow_drop_down</i></a></b>
										</th>
									</tr>
								</thead>
								<tbody id="homeworkBody{{$lesson->id}}" style="display: none;">
									<tr>
										<th>Af op:</th>
										<th>Omschrijving:</th>
										<th></th>
									</tr>
									@foreach($homeworks->where('lessonId', $lesson->id)->where('done', 0) as $homework)
										<tr>
											<td>{{ date('d-m-Y', strtotime($homework->dueDate)) }}</td>
											<td>{{ $homework->description }}</td>
											<td>
												{{-- Edit Homework Icon --}}
												<a data-target="#editHomeworkModal{{$homework->id}}" data-toggle="modal">
													<i class="material-icons tableIcon">
														edit
													</i>
												</a>

												{{-- Edit Homework Modal --}}
												<section class="modal fade" id="editHomeworkModal{{$homework->id}}" tabindex="-1" role="dialog" aria-labelledby="editHomeworkModalLabel{{$homework->id}}" aria-hidden="true">
												  <section class="modal-dialog" role="document">
												    <section class="modal-content">
												      <section class="modal-header">
												        <h5 class="modal-title" id="editHomeworkModalLabel{{$homework->id}}">Huiswerk wijzigen</h5>
												        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
												          <span aria-hidden="true">&times;</span>
												        </button>
												      </section>
												      <form method="post" action="{{ url('/homework/edit/'.$homework->id) }}">
												      	{{ csrf_field() }}
													      <section class="modal-body">
													        <section class="form-group">
													        	<label for="edithomeworkDate">Datum</label>
													        	<input type="date" class="form-control" id="edithomeworkDate" name="editHomeworkDate" value="{{ date('Y-m-d', strtotime($homework->dueDate)) }}">
													        </section>

													        <section class="form-group">
													        	<label for="editHomeworkLesson">Kies een vak</label>
													        	<select class="form-control" id="editHomeworkLesson" name="editHomeworkLesson">
													        		<option value="{{ $homework->lessonId }}" selected>{{ $homework->lesson->name }}</option>
													        		@foreach($lessons->where('id', '!=', $homework->lessonId) as $lesson)
													        			<option value="{{ $lesson->id }}">{{ $lesson->name }}</option>	
													        		@endforeach
													        	</select>
													        </section>

													        <section class="form-group">
													        	<label for="editHomeworkDescription">Omschrijving / Opdracht</label>
													        	<textarea class="form-control" placeholder="Vul hier het huiswerk in..." name="editHomeworkDescription">{{$homework->description}}</textarea>
													        </section>
													      </section>
													      <section class="modal-footer">
													        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
													        <button type="submit" class="btn btn-primary red--button" name="editHomeworkSubmit">Wijzigen</button>
													      </section>
												      </form>
												    </section>
												  </section>
												</section>	

												{{-- Delete Homework Icon--}}
												<a data-target="#deleteHomeworkModal{{$homework->id}}" data-toggle="modal">
													<i class="material-icons tableIcon">
														delete
													</i>
												</a>

												{{-- Delete Homework Modal --}}
												<section class="modal fade" id="deleteHomeworkModal{{$homework->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteHomeworkModalLabel{{$homework->id}}" aria-hidden="true">
												  <section class="modal-dialog" role="document">
												    <section class="modal-content">
												      <section class="modal-header">
												        <h5 class="modal-title" id="deleteHomeworkModalLabel{{$homework->id}}">Huiswerk verwijderen</h5>
												        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
												          <span aria-hidden="true">&times;</span>
												        </button>
												      </section>
												      <form method="get" action="{{ url('/homework/delete/'.$homework->id) }}">
													      <section class="modal-body">
													      	<p>Weet je zeker dat je het volgende huiswerk wilt verwijderen? <br />
													      	<i>{{ $homework->description }}</i></p>
													      </section>
													      <section class="modal-footer">
													        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
													        <button type="submit" class="btn btn-danger red--button" name="deleteHomeworkSubmit">Verwijderen</button>
													      </section>
												      </form>
												    </section>
												  </section>
												</section>


												{{-- Check homework --}}
												<a data-target="#checkHomeworkModal{{$homework->id}}" data-toggle="modal">
													<i class="material-icons tableIcon">
														check
													</i>
												</a>
												{{-- Check Homework Modal --}}
												<section class="modal fade" id="checkHomeworkModal{{$homework->id}}" tabindex="-1" role="dialog" aria-labelledby="checkHomeworkModalLabel{{$homework->id}}" aria-hidden="true">
												  <section class="modal-dialog" role="document">
												    <section class="modal-content">
												      <section class="modal-header">
												        <h5 class="modal-title" id="checkHomeworkModalLabel{{$homework->id}}">Huiswerk afgewerkt?</h5>
												        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
												          <span aria-hidden="true">&times;</span>
												        </button>
												      </section>
												      <form method="post" action="{{ url('/homework/check/'.$homework->id) }}">
												      	{{ csrf_field() }}
													      <section class="modal-body">
													      	<p>Heb je het volgende huiswerk afgerond? <br />
													      	<i>{{ $homework->description }}</i></p>
													      </section>
													      <section class="modal-footer">
													        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
													        <button type="submit" class="btn btn-primary red--button" name="checkHomeworkSubmit">Afwerken</button>
													      </section>
												      </form>
												    </section>
												  </section>
												</section>	
											</td>
										</tr>
									@endforeach
								</tbody>
							@endforeach
						</table>
					</section>
				</section>
			</section>
		</section>

		<section class="col-md-12 mt-3">
			<section class="card">

				{{-- Card Header --}}
				<section class="card-header">
					<h3>Afgerond huiswerk</h3>
				</section>

				{{-- Card Body --}}
				<section class="card-body">
					<section class="table-responsive">
						<table class="table table-striped">
							@foreach($lessons as $lesson)
								<thead>
									<tr>
										<th colspan="4">
											<b>{{ $lesson->name }} <a onclick="showArchive({{$lesson->id}})"><i class="material-icons tableIcon">arrow_drop_down</i></a></b>
										</th>
									</tr>
								</thead>
								<tbody id="archiveBody{{$lesson->id}}" style="display: none;">
									<tr>
										<th>Af op:</th>
										<th>Omschrijving:</th>
										<th></th>
									</tr>
									@foreach($homeworks->where('lessonId', $lesson->id)->where('done', 1) as $homework)
										<tr>
											<td>{{ date('d-m-Y', strtotime($homework->dueDate))}}</td>
											<td>{{ $homework->description }}</td>
											<td>
												{{-- Delete Homework Icon--}}
												<a data-target="#deleteArchiveHomeworkModal{{$homework->id}}" data-toggle="modal">
													<i class="material-icons tableIcon">
														delete
													</i>
												</a>
												{{-- Delete Homework Modal --}}
												<section class="modal fade" id="deleteArchiveHomeworkModal{{$homework->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteArchiveHomeworkModalLabel{{$homework->id}}" aria-hidden="true">
												  <section class="modal-dialog" role="document">
												    <section class="modal-content">
												      <section class="modal-header">
												        <h5 class="modal-title" id="deleteArchiveHomeworkModalLabel{{$homework->id}}">Huiswerk verwijderen</h5>
												        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
												          <span aria-hidden="true">&times;</span>
												        </button>
												      </section>
												      <form method="get" action="{{ url('/homework/delete/'.$homework->id) }}">
													      <section class="modal-body">
													      	<p>Weet je zeker dat je het volgende huiswerk wilt verwijderen? <br />
													      	<i>{{ $homework->description }}</i></p>
													      </section>
													      <section class="modal-footer">
													        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
													        <button type="submit" class="btn btn-danger" name="deleteHomeworkSubmit">Verwijderen</button>
													      </section>
												      </form>
												    </section>
												  </section>
												</section>
											</td>
										</tr>
									@endforeach
								</tbody>
							@endforeach
						</table>
					</section>
				</section>

			</section>
		</section>
	</section>

	{{-- Add Homework --}}
	<section class="modal fade" id="addHomeworkModal" tabindex="-1" role="dialog" aria-labelledby="addHomeworkModalLabel" aria-hidden="true">
	  <section class="modal-dialog" role="document">
	    <section class="modal-content">
	      <section class="modal-header">
	        <h5 class="modal-title" id="addHomeworkModalLabel">Huiswerk Toevoegen</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </section>
	      <form method="post" action="{{ url('/homework/add') }}">
	      	{{ csrf_field() }}
		      <section class="modal-body">
		        <section class="form-group">
		        	<label for="homeworkDate">Datum</label>
		        	<input type="date" class="form-control" id="homeworkDate" name="homeworkDate">
		        </section>

		        <section class="form-group">
		        	<label for="homeworkLesson">Kies een vak</label>
		        	<select class="form-control" id="homeworkLesson" name="homeworkLesson">
		        		<option value="" disabled>Vakken</option>
		        		@foreach($lessons as $lesson)
		        			<option value="{{ $lesson->id }}">{{ $lesson->name }}</option>	
		        		@endforeach
		        	</select>
		        </section>

		        <section class="form-group">
		        	<label for="homeworkDescription">Omschrijving / Opdracht</label>
		        	<textarea class="form-control" placeholder="Vul hier het huiswerk in..." name="homeworkDescription"></textarea>
		        </section>
		      </section>
		      <section class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
		        <button type="submit" class="btn btn-primary red--button" name="homeworkSubmit">Toevoegen</button>
		      </section>
	      </form>
	    </section>
	  </section>
	</section>

@endsection

<script>
        window.onload = function() {
            setTimeout(function(){  document.getElementById('sessionMessage').style.display = "none"; }, 3000);
        };

	function showTable(lessonId) {
		var tableBody = document.getElementById("homeworkBody" + lessonId);
		if (tableBody.style.display == "none") {
			tableBody.style.display = "";
		} else {
			tableBody.style.display = "none";
		}
	}
	function showArchive(lessonId) {
		var tableBody = document.getElementById("archiveBody" + lessonId);
		if (tableBody.style.display == "none") {
			tableBody.style.display = "";
		} else {
			tableBody.style.display = "none";
		}
	}
</script>