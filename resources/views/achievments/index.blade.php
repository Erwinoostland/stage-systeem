@extends('layouts.app')

@section('content')

	<section class="container-fluid">
		<section class="row">
			<section class="col-md-12 mt-85">
				<h3>Behaalde achievments</h3>
				<hr />

				{{-- All achievments --}}
				<section class="row">
					@php $achievmentIds = []; @endphp
					@foreach ($allAchievmentProgressions as $achievmentProgression)
					@php array_push($achievmentIds, $achievmentProgression->id) @endphp
						<section class="col-md-4">
							<section class="card mt-2 card-hover">
								<section class="card-body">
									<section class="row">
										{{-- Image/Badge --}}
										<section class="col-md-4">
											<section class="profile--picture"></section>
										</section>

										{{-- Title and descripption --}}
										<section class="col-md-8">
											<h5>{{ $achievmentProgression->achievment->name }}</h5>
											<p>{{ $achievmentProgression->achievment->description }}</p>
											<i>Behaald op: {{ date('d-m-Y', strtotime($achievmentProgression->created_at)) }}</i>
										</section>

									</section>
								</section>
							</section>
						</section>
					@endforeach
				</section>

			</section>
		</section>

		<section class="row">
			<section class="col-md-12 mt-3">
				<h3>Nog te behalen achievments</h3>
				<hr />	

				{{-- All achievments --}}
				<section class="row">
					@foreach ($allAchievments->whereNotIn('id', $achievmentIds) as $achievment)
						<section class="col-md-4">
							<section class="card mt-2 card-hover">
								<section class="card-body">
									<section class="row">
										{{-- Image/Badge --}}
										<section class="col-md-4">
											<section class="profile--picture"></section>
										</section>

										{{-- Title and descripption --}}
										<section class="col-md-8">
											<h5>{{ $achievment->name }}</h5>
											<i>Nog te behalen...</i>
										</section>

									</section>
								</section>
							</section>
						</section>
					@endforeach
				</section>

			</section>
		</section>
	</section>
@endsection