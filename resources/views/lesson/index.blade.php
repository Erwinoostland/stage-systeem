@extends('layouts.app')

@section('content')
    <section class="row">
        <section class="col-md-12 mt-3">
            <section class="card">
                {{-- card header --}}
                <section class="card-header">
                    <h3>Vakken
                        <a data-toggle="modal" data-target="#addLessonModal" class="red-icon">
                            <span class="float-right">
                                <i class="material-icons tableIcon">add</i>
                            </span>
                        </a>
                    </h3>
                </section>
                    <section class="card-body">
                        @if(\Session::has('success'))
                            <p id="sessionMessage" class="alert alert-success">{{\Session::get('success')}}</p>
                        @endif
                            @if(\Session::has('danger'))
                                <p id="sessionMessage" class="alert alert-success">{{\Session::get('danger')}}</p>
                            @endif
                    @foreach($lessons as $lesson)
                    <section class="card mt-2">
                        <section class="card-header">
                            <h3>{{$lesson->name}}
                                <a data-toggle="modal" data-target="#deleteLessonModal{{$lesson->id}}" class="red-icon">
                            <span class="float-right">
                                <i class="material-icons tableIcon">delete</i>
                            </span>
                                </a>
                                <a data-toggle="modal" data-target="#editLessonModal{{$lesson->id}}" class="red-icon">
                            <span class="float-right">
                                <i class="material-icons tableIcon">edit</i>
                            </span>
                                </a>
                            </h3>
                        </section>
                    </section>
                            <!-- Delete Modal -->
                                <section class="modal fade" id="deleteLessonModal{{$lesson->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteLessonModalLabel" aria-hidden="true">
                                    <section class="modal-dialog" role="document">
                                        <section class="modal-content">
                                            <section class="modal-header">
                                                <h5 class="modal-title" id="deleteLessonModalLabel{{$lesson->id}}">Vak vewijderen</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </section>
                                            <form method="get" action="{{ url('/lessons/delete/'.$lesson->id)}}">
                                                <section class="modal-body">
                                                    <section class="form-group">
                                                        <p>Weet je zeker dat je de vak wil verwijderen?<br>
                                                            Alle huiswerk, cijfers en bestanden van dit vak worden verwijderd!
                                                        </p>
                                                    </section>
                                                </section>
                                                <section class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                    <button type="submit" class="btn btn-danger red--button">Delete</button>
                                                </section>
                                            </form>
                                        </section>
                                    </section>
                                </section>

                                <!-- Edit Modal -->
                                <section class="modal fade" id="editLessonModal{{$lesson->id}}" tabindex="-1" role="dialog" aria-labelledby="editLessonModalLabel" aria-hidden="true">
                                    <section class="modal-dialog" role="document">
                                        <section class="modal-content">
                                            <section class="modal-header">
                                                <h5 class="modal-title" id="editLessonModalLabel{{$lesson->id}}">Vak bewerken</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </section>
                                            <form method="post" action="{{ url('/lessons/edit/'.$lesson->id) }}">
                                                @csrf
                                                <section class="modal-body">
                                                    <section class="form-group">
                                                        <label for="Lesson name">Vak naam</label>
                                                        <input type="text" class="form-control" name="editLessonName" id="editLessonName" value="{{$lesson->name}}">
                                                    </section>
                                                </section>
                                                <section class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                    <button type="submit" class="btn btn-primary red--button">Wijzigen</button>
                                                </section>
                                            </form>
                                        </section>
                                    </section>
                                </section>
                    @endforeach
                </section>
            </section>
        </section>
    </section>

    <!-- Add Modal -->
    <section class="modal fade" id="addLessonModal" tabindex="-1" role="dialog" aria-labelledby="addLessonModalLabel" aria-hidden="true">
        <section class="modal-dialog" role="document">
            <section class="modal-content">
                <section class="modal-header">
                    <h5 class="modal-title" id="addLessonModalLabel">Vak toevoegen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </section>
                <form method="post" action="{{ url('/lessons/add') }}">
                    @csrf
                    <section class="modal-body">
                        <section class="form-group">
                            <label for="Lesson name">Vak naam</label>
                            <input type="text" class="form-control" name="lessonName" id="lessonName">
                        </section>
                    </section>
                    <section class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                        <button type="submit" class="btn btn-primary red--button">Toevoegen</button>
                    </section>
                </form>
            </section>
        </section>
    </section>



@endsection

<script>
    window.onload = function() {
        setTimeout(function () {
            document.getElementById('sessionMessage').style.display = "none";
        }, 3000);
    };
</script>