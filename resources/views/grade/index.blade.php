@extends('layouts.app')

@section('content')
    <section class="row">
        <section class="col-md-12 mt-3">
            <section class="card">

                {{-- Card Header --}}
                <section class="card-header">
                    <h4>Cijfers<span class="float-right"><a data-toggle="modal" data-target="#addGrade"><i
                                        class="material-icons tableIcon">add</i></a></span></h4>

                </section>

                {{-- Card Body --}}
                <section class="card-body">

                    @if(\Session::has('success'))
                        <p class="alert alert-success">{{\Session::get('success')}}</p>
                    @endif

                    @if(\Session::has('danger'))
                        <p class="alert alert-danger">{{\Session::get('danger')}}</p>
                    @endif
                    <section class="table-responsive">
                        <table class="table table-striped">
                            @foreach($lessons as $lesson)
                                <thead>
                                <tr>
                                    <th colspan="4">{{$lesson->name}}<a onclick="showBody({{$lesson->id}})"><i
                                                    class="material-icons tableIcon">arrow_drop_down</i></a></th>
                                </tr>
                                </thead>
                                <tbody id="gradeBody{{$lesson->id}}" style="display: none;">
                                <tr>
                                    <th>Behaald op:</th>
                                    <th>Omschrijving</th>
                                    <th>Cijfer</th>
                                    <th></th>
                                </tr>
                                @foreach($grades->where('lessonId', $lesson->id) as $grade)
                                    <tr>
                                        <td>{{date('d-m-Y', strtotime($grade->created_at))}}</td>
                                        <td>{{$grade->description}}</td>
                                        <td>{{number_format($grade->grade, 1)}}</td>
                                        <td>
                                            <a data-toggle="modal" data-target="#deleteGrade{{$grade->id}}"><i
                                                        class="material-icons tableIcon">
                                                    delete
                                                </i></a>
                                            {{--Delete modal--}}
                                            <section class="modal fade" id="deleteGrade{{$grade->id}}" tabindex="-1"
                                                     role="dialog" aria-labelledby="deleteGrade"
                                                     aria-hidden="true">
                                                <section class="modal-dialog" role="document">
                                                    <section class="modal-content">
                                                        <section class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Cijfer
                                                                verwijderen</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </section>
                                                        <form method="get"
                                                              action="{{url('grades/delete/' . $grade->id)}}">
                                                            <section class="modal-body">
                                                                Weet u zeker dat u deze cijfer wilt verwijderen?
                                                            </section>
                                                            <section class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Sluiten
                                                                </button>
                                                                <button type="submit"
                                                                        class="btn btn-danger red--button">
                                                                    Verwijderen
                                                                </button>
                                                            </section>
                                                        </form>
                                                    </section>
                                                </section>
                                            </section>
                                            <a data-toggle="modal" data-target="#editGrade{{$grade->id}}"><i
                                                        class="material-icons tableIcon">
                                                    edit
                                                </i></a>

                                            {{--Edit modal--}}
                                            <section class="modal fade" id="editGrade{{$grade->id}}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="editGrade"
                                                     aria-hidden="true">
                                                <section class="modal-dialog" role="document">
                                                    <section class="modal-content">
                                                        <section class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Cijfer
                                                                wijzigen</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </section>
                                                        <form method="post"
                                                              action="{{url('grades/edit/'. $grade->id)}}">
                                                            {{csrf_field()}}
                                                            <section class="modal-body">
                                                                <label for="editGradeLesson">Vak:</label>
                                                                <select name="editGradeLesson" class="form-control">
                                                                    <option value="" disabled>Kies één</option>
                                                                    @foreach($lessons->where('id') as $lesson)
                                                                        <option value="{{ $lesson->id }}"> {{ $lesson->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <section class="form-group">
                                                                    <label for="editDescription">Omschrijving:</label>
                                                                    <textarea class="form-control"
                                                                              name="editGradeDescription"
                                                                              id="editDescription">{{$grade->description}}</textarea>
                                                                </section>
                                                                <section class="form-group">
                                                                    <label for="editGrade">Cijfer:</label>
                                                                    <input name="editGrade" id="editGrade" type="number"
                                                                           step=".01"
                                                                           class="form-control"
                                                                           value="{{$grade->grade}}">
                                                                </section>
                                                            </section>
                                                            <section class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Sluiten
                                                                </button>
                                                                <button type="submit" name="editGradeSubmit"
                                                                        class="btn btn-primary red--button">
                                                                    Wijzigen
                                                                </button>
                                                            </section>
                                                        </form>
                                                    </section>
                                                </section>
                                            </section>
                                    </tr>

                                @endforeach
                                </tbody>
                            @endforeach
                        </table>
                    </section>
                </section>
            </section>
        </section>
    </section>

    {{--Add modal--}}
    <section class="modal fade" id="addGrade" tabindex="-1" role="dialog" aria-labelledby="addGrade" aria-hidden="true">
        <section class="modal-dialog" role="document">
            <section class="modal-content">
                <section class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Cijfer toevoegen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </section>
                <form method="post" action="{{ url('/grades/add') }}">
                    {{csrf_field()}}
                    <section class="modal-body">
                        <section class="form-group">
                            <label for="lesson">Vak</label>
                            <select name="addLesson" class="form-control">
                                <option value="" disabled>Kies één</option>
                                @foreach($lessons as $lesson)
                                    <option value="{{ $lesson->id }}"> {{ $lesson->name }}</option>
                                @endforeach
                            </select>
                        </section>
                        <section class="form-group">
                            <label for="addDescription">Omschrijving:</label>
                            <textarea class="form-control" name="addDescription"
                                      id="addDescription"></textarea>
                        </section>
                        <section class="form-group">
                            <label for="addGrade">Cijfer:</label>
                            <input name="addGrade" type="number" step=".01" id="addGrade" class="form-control">
                        </section>
                        <section class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                            <button type="submit" class="btn btn-danger red--button">Toevoegen</button>
                        </section>
                    </section>
                </form>
            </section>
        </section>
    </section>

@endsection


<script>
    function showBody(lessonId) {
        var tableBody = document.getElementById("gradeBody" + lessonId);
        if (tableBody.style.display == "none") {
            tableBody.style.display = "";
        } else {
            tableBody.style.display = "none";
        }
    }
</script>
