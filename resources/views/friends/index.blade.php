@extends('layouts.app')
@section('content')
	<section class="row">
		<section class="col-md-12 mt-100">
			<p>Aantal vrienden: <span>{{ $countFriends }}</span></p>
			<hr />

			{{-- Session messages --}}
			@if(\Session::has('danger'))
				<p class="alert alert-danger">{{ \Session::get('danger') }}</p>

			@elseif(\Session::has('success'))
				<p class="alert alert-success">{{ \Session::get('success') }}</p>
			@endif

			{{-- Loop through all friends --}}
			<section class="row"> 
			@foreach($friends->where('accepted', 1) as $friend)
				<section class="col-md-6">
					<section class="card">
						<section class="body">
							<section class="row">
								{{-- Profile Picture --}}
								<section class="col-md-3">
									<section class="profile--picture p-3">
										
									</section>
								</section>

								{{-- Information --}}
								<section class="col-md-7">
									<section class="friend--info p-3">
										<p>
											{{-- Friend name --}}
											@if($friend->userId == \Auth::id())
												@if(!empty($friend->friend->firstname) && !empty($friend->friend->lastname))
													Naam: <b>{{ $friend->friend->firstname . " " . $friend->friend->lastname}}</b>
													<br />
												@else
													Naam: <b><i>Niet bekend..</i></b>
													<br />
												@endif
											@else
												@if(!empty($friend->user->firstname) && !empty($friend->user->lastname))
													Naam: <b>{{ $friend->user->firstname . " " . $friend->user->lastname}}</b>
													<br />
												@else
													Naam: <b><i>Niet bekend..</i></b>
													<br />
												@endif
											@endif
											
											{{-- Email --}}
											@if($friend->userId == \Auth::id())
												@if(!empty($friend->friend->email))
													Email: <b>{{ $friend->friend->email }}</b>
													<br />
												@else
													Email: <b><i>Niet bekend..</i></b>
													<br />
												@endif
											@else
												@if(!empty($friend->user->email))
													Email: <b>{{ $friend->user->email }}</b>
													<br />
												@else
													Email: <b><i>Niet bekend..</i></b>
													<br />
												@endif
											@endif

											{{-- Date of Birth --}}
											@if($friend->userId == \Auth::id())
												@if(!empty($friend->friend->dateOfBirth))
													Geboortedatum: <b>{{ date('d-m-Y', strtotime($friend->friend->dateOfBirth)) }}</b>
													<br />
												@else
													Geboortedatum: <b><i>Niet bekend..</i></b>
													<br />
												@endif
											@else
												@if(!empty($friend->user->dateOfBirth))
													Geboortedatum: <b>{{ date('d-m-Y', strtotime($friend->user->dateOfBirth)) }}</b>
													<br />
												@else
													Geboortedatum: <b><i>Niet bekend..</i></b>
													<br />
												@endif
											@endif

											{{-- Date of friendship --}}
											@if(!empty($friend->updated_at))
												Vrienden vanaf: <b>{{ date('d-m-Y', strtotime($friend->updated_at)) }}</b>
											@else
												Vrienden vanaf: <b><i>Niet bekend..</i></b>
												<br />
											@endif
										</p>
									</section>
								</section>
								<section class="col-md-2 mt-2">
									<a data-toggle="modal" data-target="#deleteFriend{{$friend->id}}">
										<i class="material-icons tableIcon delete--icon">
											delete
										</i>
									</a>

									{{-- Delete FriendRequest Modal --}}
									<section class="modal fade" id="deleteFriend{{$friend->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteFriendLabel{{$friend->id}}" aria-hidden="true">
									  <section class="modal-dialog" role="document">
									    <section class="modal-content">
									      <section class="modal-header">
									        <h5 class="modal-title" id="deleteFriendLabel{{$friend->id}}">Vriend verwijderen</h5>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">&times;</span>
									        </button>
									      </section>
									      <form method="get" action="{{ url('/friends/delete/'.$friend->id) }}">
										      <section class="modal-body">
										      	@if ($friend->friendId == \Auth::id())
									        		<p>
									        			Wil je {{ $friend->user->firstname . " " . $friend->user->lastname }} verwijderen als vriend?
									        		</p>
									        	@else
									        		<p>
									        			Wil je {{ $friend->friend->firstname . " " . $friend->friend->lastname }} verwijderen als vriend?
									        		</p>
									        	@endif
										      </section>
										      <section class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
										        <button type="submit" class="btn btn-danger red--button" name="deleteHomeworkSubmit">Verwijderen</button>
										      </section>
									      </form>
									    </section>
									  </section>
									</section>
								</section>
							</section>
						</section>
					</section>
				</section>
			@endforeach
		</section>

		{{-- Open requests --}}
		<section class="row">
			<section class="col-md-6 mt-3">
				<section class="card">
					<section class="card-header"><h4>Vriendschapverzoeken</h4></section>
					<section class="card-body">
						@if(empty($openRequests[0]))
							<i>Je hebt geen Vriendschapverzoeken..</i>
						@endif
						@foreach($openRequests as $openRequest)
						<section class="row">
							<section class="col-md-1">
								<section class="profile--picture--small p-3">
											
								</section>
							</section>
							<section class="col-md-11">
								<section class="friend--info p-3">
									<p>
										<b>{{ $openRequest->user->firstname }} {{ $openRequest->user->lastname }}</b> wil vrienden met je worden.
										<a data-toggle="modal" data-target="#deleteFriendRequest{{$openRequest->id}}">
											<i class="material-icons tableIcon float-right">
												delete
											</i>
										</a>

										<a data-toggle="modal" data-target="#acceptFriend{{$openRequest->id}}">
											<i class="material-icons tableIcon float-right">
												check
											</i>
										</a>
									</p>

									{{-- Delete Friend Modal --}}
									<section class="modal fade" id="deleteFriendRequest{{$openRequest->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteFriendRequestLabel{{$openRequest->id}}" aria-hidden="true">
									  <section class="modal-dialog" role="document">
									    <section class="modal-content">
									      <section class="modal-header">
									        <h5 class="modal-title" id="deleteFriendRequestLabel{{$openRequest->id}}">Vriendschapverzoek afwijzen</h5>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">&times;</span>
									        </button>
									      </section>
									      <form method="get" action="{{ url('/friends/delete/'.$openRequest->id) }}">
										      <section class="modal-body">
										      		<p>Wil je de vriendschap verzoek van {{ $openRequest->user->firstname . " " . $openRequest->user->lastname }} afwijzen?</p>
										      </section>
										      <section class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
										        <button type="submit" class="btn btn-danger red--button">Afwijzen</button>
										      </section>
									      </form>
									    </section>
									  </section>
									</section>

									{{-- Accept Request Modal --}}
									<section class="modal fade" id="acceptFriend{{$openRequest->id}}" tabindex="-1" role="dialog" aria-labelledby="acceptFriendLabel{{$openRequest->id}}" aria-hidden="true">
									  <section class="modal-dialog" role="document">
									    <section class="modal-content">
									      <section class="modal-header">
									        <h5 class="modal-title" id="acceptFriendLabel{{$openRequest->id}}">Vriendschapverzoek accepteren</h5>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">&times;</span>
									        </button>
									      </section>
									      <form method="post" action="{{ url('/friends/accept/'.$openRequest->id) }}">
									      	@csrf
										      <section class="modal-body">
										      	<p>Wil je de vriendschap verzoek van {{ $openRequest->user->firstname . " " . $openRequest->user->lastname }} accepteren?</p>
										      </section>
										      <section class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
										        <button type="submit" class="btn btn-danger red--button">Accepteren</button>
										      </section>
									      </form>
									    </section>
									  </section>
									</section>
								</section>
							</section>
						</section>
						@endforeach
					</section>
				</section>
			</section>

			{{-- Outgoing requests --}}
			<section class="col-md-6 mt-3">
				<section class="card">
					<section class="card-header"><h4>Uitgaande Vriendschapverzoeken</h4></section>
					<section class="card-body">
						@if(empty($outgoingRequests[0]))
							<i>Je hebt geen uitgaande Vriendschapverzoeken..</i>
						@endif
						@foreach($outgoingRequests as $outgoingRequest)
							<p>{{ $outgoingRequest->friend->firstname }}</p>
						@endforeach
					</section>
				</section>
			</section>
		</section>
	</section>
</section>	
@endsection