<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<section id="app">
    <nav class="material--navbar navbar navbar-expand-md navbar-light navbar-laravel">
        <section class="container-fluid">
            <button onclick="sideDrawerToggle()" id="drawerToggle" class="navbar-icon--button material-icons">menu
            </button>

            <a class="navbar-brand" href="{{ url('/') }}">
                Schoolsysteem
            </a>
            {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"--}}
            {{--aria-controls="navbarSupportedContent" aria-expanded="false"--}}
            {{--aria-label="{{ __('Toggle navigation') }}">--}}
            {{--<span class="navbar-toggler-icon"></span>--}}
            {{--</button>--}}

            <section class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                    @else
                        <a href="{{url('profile')}}">
                            <img class="navbar-profile--image" src="https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1441527/1160/772/m1/fpnw/wm0/businessman-avatar-icon-01-.jpg?1468234792&s=e3a468692e15e93a2056bd848193e97a">
                        </a>
                        <a href="{{ route('logout') }}"
                           class="navbar-icon--button material-icons">exit_to_app</a>
                    @endguest
                </ul>
            </section>
        </section>
    </nav>
    {{--<header class="top-header">--}}
    {{--<section class="top-header--row">--}}
    {{--<button class="top-header--button material-icons">menu</button>--}}
    {{--<span class="top-header--row__text">Schoolsysteem</span>--}}
    {{--</section>--}}
    {{--<section class="top-header--row__right">--}}
    {{--<i class="material-icons">--}}
    {{--supervised_user_circle--}}
    {{--</i>--}}
    {{--</section>--}}
    {{--</header>--}}

    @if(\Auth::user())
        <section class="aside">
            <section id="sideDrawer" class="drawer drawer--active">
                <a class="drawer--listitem" href="{{ url('/') }}">
                    <span class="drawer--listitem-icon"><i class="material-icons">home</i></span>

                    <span class="drawer--text"> Overzicht</span>
                </a>
                <a class="drawer--listitem" href="{{ url('/homework') }}">
                <span class="drawer--listitem-icon"> <i class="material-icons">
                    insert_drive_file
                </i></span>
                    <span class="drawer--text"> Huiswerk</span>
                </a>
                <a class="drawer--listitem" href="{{ url('/grades') }}">
                <span class="drawer--listitem-icon"> <i class="material-icons">
                    format_list_numbered
                </i></span>

                    <span class="drawer--text"> Cijfers</span>
                </a>
                <a class="drawer--listitem" href="{{ url('/file') }}">
                    <i class="material-icons">
                        attach_file
                    </i>
                    <span class="drawer--text"> Bestanden</span>
                </a>
                <a class="drawer--listitem" href="{{ url('/calendar') }}">
                    <i class="material-icons">
                        calendar_today
                    </i>
                    <span class="drawer--text"> Agenda</span>
                </a>
                <a class="drawer--listitem" href="{{ url('/friends') }}">
                    <i class="material-icons">
                        supervised_user_circle
                    </i>
                    <span class="drawer--text"> Vrienden</span>
                </a>
                <a class="drawer--listitem" href="{{ url('/achievments') }}">
                    <i class="material-icons">
                        stars
                    </i>
                    <span class="drawer--text"> Achievements</span>
                </a>
                <a class="drawer--listitem" href="{{ url('/lessons') }}">
                    <i class="material-icons">
                        all_inbox
                    </i>
                    <span class="drawer--text"> Vakken</span>
                </a>
                <section class="drawer--listitem">
                    <form class="drawer--search" method="get" action="{{ url('/search') }}">
                        <input type="search" placeholder="Zoek iemand..." name="searchInput">
                        <button type="submit" class="material-icons">search</button>
                    </form>
                </section>
            </section>
        </section>
    @endif
    <section class="@if(\Auth::user()) layout--dashboard layout--fluid @endif">
        <section class="container-fluid">
            @yield('content')
        </section>
    </section>
    <script>
        function sideDrawerToggle() {

            const drawer = document.querySelector(".drawer");
            const layout = document.querySelector(".layout--dashboard");

            if (drawer.classList.contains('drawer--active')) {
                drawer.classList.remove('drawer--active');
                layout.classList.remove('layout--fluid')
            } else {
                drawer.classList.add('drawer--active');
                layout.classList.add('layout--fluid');
            }
        }


    </script>
</section>
</body>
</html>
