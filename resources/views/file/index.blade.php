@extends('layouts.app')

@section('content')
    <section class="row">
        <section class="col-md-12 mt-3">
            <section class="card">

                {{-- Card Header --}}
                <section class="card-header">
                    <h3>Persoonlijke bestanden
                        <a href="" data-toggle="modal" data-target="#addFileModal" class="red--icon">
							<span class="float-right">
								<i class="material-icons">add</i>
							</span>
                        </a>
                    </h3>
                </section>

                {{-- Card Body --}}
                <section class="card-body">

                    {{-- Session Flash Messages --}}
                    @if(Session::has('success'))
                        <p id="sessionMessage" class="alert alert-success">{{ Session::get('success') }}</p>
                    @endif

                    {{-- Session Flash Messages --}}
                    @if(Session::has('danger'))
                        <p id="sessionMessage" class="alert alert-danger">{{ Session::get('danger') }}</p>
    @endif
                    <section class="table-responsive">
                        <table class="table table-striped">
                            @foreach($lessons as $lesson)
                                <thead>
                                <tr>
                                    <th colspan="4">{{$lesson->name}}<a onclick="showBody({{$lesson->id}})"><i
                                                    class="material-icons tableIcon">arrow_drop_down</i></a></th>
                                </tr>
                                </thead>

                                <tbody id="fileBody{{$lesson->id}}" style="display: none;">
                                <tr>
                                    <th>Bestandnaam:</th>
                                    <th>Download:</th>
                                    <th></th>
                                </tr>
                                @foreach($files->where('lessonId', $lesson->id) as $file)
                                <tr>
                                    <td>{{$file->fileName}}</td>
                                    <td>
                                        <a href="{{asset('files/' . \Auth::id() . '-' . \Auth::user()->firstname . '-'. \Auth::user()->lastname.'/' . $lesson->id .'-' . $lesson->name .'/'.$file->filePath)}}" download>{{$file->filePath}}</a>

                                    </td>
                                <td>
                                    {{-- Edit File Icon --}}
                                    <a data-target="#editFileModal{{$file->id}}" data-toggle="modal">
                                        <i class="material-icons tableIcon">
                                            edit
                                        </i>
                                    </a>

                                    {{-- Edit File Modal --}}
                                    <section class="modal fade" id="editFileModal{{$file->id}}" tabindex="-1" role="dialog" aria-labelledby="editFileModalLabel{{$file->id}}" aria-hidden="true">
                                        <section class="modal-dialog" role="document">
                                            <section class="modal-content">
                                                <section class="modal-header">
                                                    <h5 class="modal-title" id="editFileModalLabel{{$file->id}}">Bestand bewerken</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </section>
                                                <form method="post" action="{{ url('/file/edit/'.$file->id) }}">
                                                    @csrf
                                                    <section class="modal-body">
                                                        <section class="form-group">
                                                            <label for="File name">Bestand naam</label>
                                                            <input type="text" class="form-control" name="editFileName" id="editFileName" value="{{$file->fileName}}">
                                                        </section>
                                                    </section>
                                                    <section class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                        <button type="submit" class="btn btn-primary red--button">Wijzigen</button>
                                                    </section>
                                                </form>
                                            </section>
                                        </section>
                                    </section>

                                    {{-- Delete File Icon--}}
                                    <a data-target="#deleteFileModal{{$file->id}}" data-toggle="modal">
                                        <i class="material-icons tableIcon">
                                            delete
                                        </i>
                                    </a>

                                    {{-- Delete File Modal --}}
                                    <section class="modal fade" id="deleteFileModal{{$file->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteFileModalLabel{{$file->id}}" aria-hidden="true">
                                        <section class="modal-dialog" role="document">
                                            <section class="modal-content">
                                                <section class="modal-header">
                                                    <h5 class="modal-title" id="deleteFileModalLabel{{$file->id}}">Bestand verwijderen</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </section>
                                                <form method="get" action="{{ url('/file/delete/'.$file->id) }}">
                                                    <section class="modal-body">
                                                        <p>Weet je zeker dat je het bestand ({{$file->fileName}}) wilt verwijderen? <br />
                                                            <i></i></p>
                                                    </section>
                                                    <section class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                        <button type="submit" class="btn btn-danger" name="deleteFileSubmit">Verwijderen</button>
                                                    </section>
                                                </form>
                                            </section>
                                        </section>
                                    </section>
                                </td>
                                </tr>
                                @endforeach
                                </tbody>

                            @endforeach
                        </table>
                    </section>
                </section>
            </section>
        </section>
    </section>


@endsection

{{-- Add File Modal --}}
<section class="modal fade" id="addFileModal" tabindex="-1" role="dialog" aria-labelledby="addFileModalLabel" aria-hidden="true">
    <section class="modal-dialog" role="document">
        <section class="modal-content">
            <section class="modal-header">
                <h5 class="modal-title" id="addFileModalLabel}">Bestand toevoegen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </section>
            <form method="post" action="{{ url('/file/add/') }}" enctype="multipart/form-data">
                @csrf
                <section class="modal-body">
                    <section class="form-group">
                        <label for="File name">Bestand naam</label>
                        <input type="text" class="form-control" name="addFileName" id="addFileName" value="">
                    </section>
                    <section class="form-group">
                        <label for="fileLesson">Kies een vak</label>
                        <select class="form-control" id="fileLesson" name="fileLesson">
                            <option value="" disabled selected>Kies één</option>
                            @foreach($lessons as $lesson)
                                <option value="{{ $lesson->id }}">{{ $lesson->name }}</option>
                            @endforeach
                        </select>
                    </section>
                    <input type="file" name="uploadFile" class="uploadButton">
                </section>
                <section class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                    <button type="submit" class="btn btn-primary red--button">Toevoegen</button>
                </section>
            </form>
        </section>
    </section>
</section>

<script>
    window.onload = function() {
        setTimeout(function(){  document.getElementById('sessionMessage').style.display = "none"; }, 3000);
    };
    function showBody(lessonId) {
        var tableBody = document.getElementById("fileBody" + lessonId);
        if (tableBody.style.display == "none") {
            tableBody.style.display = "";
        } else {
            tableBody.style.display = "none";
        }
    }
</script>