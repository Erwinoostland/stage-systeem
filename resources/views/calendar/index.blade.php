@extends('layouts.app')

@section('content')
    <section class="row">
        <section class="col-md-12 mt-85">
            <h4>Weeknummer: {{ $thisWeek }}</h4>
            <section class="row">
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Maandag {{ date('d-m', strtotime($firstDayOfWeek)) }}</b>

                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Dinsdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfWeek)->addDays(1))) }}</b>
                        </section>
                        <section class="card-body">
                            <ul>
                                <li>test1</li>
                                <li>test2</li>
                                <li>test3</li>
                            </ul>
                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Woensdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfWeek)->addDays(2))) }}</b>

                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Donderdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfWeek)->addDays(3))) }}</b>

                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Vrijdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfWeek)->addDays(4))) }}</b>

                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Zaterdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfWeek)->addDays(5))) }}</b>

                        </section>
                    </section>
                </section>
            </section>
        </section>
        <section class="col-md-12 mt-2">
            <hr />
            <h4>Weeknummer: {{ $nextWeek }}</h4>
            <section class="row">
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Maandag {{ date('d-m', strtotime($firstDayOfNextWeek)) }}</b>

                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Dinsdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfNextWeek)->addDays(1))) }}</b>

                        </section>
                        <section class="card-body">
                            <ul>
                                <li>test1</li>
                                <li>test2</li>
                                <li>test3</li>
                            </ul>
                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Woensdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfNextWeek)->addDays(2))) }}</b>

                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Donderdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfNextWeek)->addDays(3))) }}</b>

                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Vrijdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfNextWeek)->addDays(4))) }}</b>

                        </section>
                    </section>
                </section>
                <section class="col-md-2">
                    <section class="card mt-2">
                        <section class="card-header">
                            <b>Zaterdag {{ date('d-m', strtotime(\Carbon\Carbon::parse($firstDayOfNextWeek)->addDays(5))) }}</b>

                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>
@endsection