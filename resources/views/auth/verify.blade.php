@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Vul je email-adres in.') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Een verificatie link is verzonden naar dit email-adres.') }}
                        </div>
                    @endif

                    {{ __('Voordat je verder gaat, check je email voor een verificatie link.') }}
                    {{ __('Heb je geen verificatie link ontvangen?') }}, <a href="{{ route('verification.resend') }}">{{ __('klik hier om nog een te verzenden.') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
