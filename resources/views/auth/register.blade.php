@extends('layouts.app')

@section('content')
    <section class="login--box">
        <section class="col-md-4">
            <section class="login-heading">
                <h1>Schoolsysteem</h1>
            </section>
            <section class="login-box">
                <section class="login--box-heading">
                    <h1>Registreren</h1>
                </section>
                <section class="login--box-heading--icon">
                    <section class="login--icon">
                        <i class="material-icons">edit</i>
                    </section>
                </section>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <input id="name" type="text" placeholder="Voornaam" class="material-input" name="firstname"
                           value="{{ old('firstname') }}" required autofocus>

                    @if ($errors->has('firstname'))
                        <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('firstname') }}</strong>
            </span>
                    @endif

                    <input id="name" type="text" placeholder="Achternaam"
                           class="material-input{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname"
                           value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('lastname'))
                        <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('lastname') }}</strong>
            </span>
                    @endif

                    <input id="email" type="email" placeholder="Email"
                           class="material-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                           value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
                    @endif

                    <input id="password" type="password" placeholder="Wachtwoord"
                           class="material-input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
                    @endif

                    <input id="password-confirm" type="password" placeholder="Herhaal wachtwoord" class="material-input"
                           name="password_confirmation" required>

                    <button type="submit" class="material--button-flat">
                        {{ __('Registreren') }}
                    </button>


                </form>
            </section>

        </section>
    </section>

@endsection
