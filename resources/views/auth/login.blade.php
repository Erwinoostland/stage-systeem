@extends('layouts.app')

@section('content')
    <section class="login--box">
        <section class="col-md-4">
            <section class="login-heading">
                <h1>Schoolsysteem</h1>
            </section>

            <section class="login-box">
                <section class="login--box-heading">
                    <h1>Login</h1>
                </section>
                <section class="login--box-heading--icon">
                    <section class="login--icon">
                        <i class="material-icons">edit</i>
                    </section>
                </section>

                <form method="POST" action="{{ route('login') }}">

                    @csrf
                    <input id="email" type="email" placeholder="Email adres"
                           class="material-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                           value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
                    @endif


                    <input id="password" type="password" placeholder="Wachtwoord"
                           class="material-input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
                    @endif


                    {{--<input class="form-check-input" type="checkbox" name="remember"--}}
                    {{--id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                    {{--<label class="form-check-label" for="remember">--}}
                    {{--{{ __('Remember Me') }}--}}
                    {{--</label>--}}


                    <button type="submit" class="material--button-flat">{{ __('Login') }}</button>
                    <p><a class="material--button-link" href="{{ route('password.request') }}">
                            {{ __('Wachtwoord vergeten?') }} </a></p>
                    <p><a class="material--button-link" href="{{ route('register') }}">{{ __('Nog geen account? Registreer!') }}</a></p>

                </form>
            </section>
        </section>
    </section>




@endsection
