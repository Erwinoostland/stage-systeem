@extends('layouts.app')
@section('content')
    <section class="row">
        <section class="offset-2 col-md-8">
            <section class="profile-banner">
                <img class="profile-banner--image"
                     src="https://www.acurax.com/wp-content/themes/acuraxsite/images/inner_page_bnr.jpg?x21789">
            </section>
            <section class="profile-icon">
                <img class="profile-icon--image"
                     src="https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1441527/1160/772/m1/fpnw/wm0/businessman-avatar-icon-01-.jpg?1468234792&s=e3a468692e15e93a2056bd848193e97a">
                <h1>{{$user->firstname}} {{$user->lastname}}</h1>
            </section>
        </section>

    </section>
    <section class="row">
        <section class="offset-2 col-md-8">
            <section class="button-container">
                <section class="profile-buttons">
                    <button id="userDataBtn" onclick=" transformInDataSection()" class="profile--button">Persoonlijke gegevens</button>
                    <button id="ChangeDataBtn" onclick="transformInSettingsSection()" class="profile--button">Instellingen</button>
                </section>
            </section>
        </section>
    </section>
    <section class="row">
        <section id="DataSection" class="offset-2 col-md-8">
            <section class="paper-card">
                <section class="row">
                    <section class="col-md-4">
                        <h5>Naam:</h5>
                        <h5>Geboortedatum:</h5>
                        <h5>Leerlingnummer:</h5>
                        <h5>Schoolemail:</h5>
                        <h5>Aantal Vrienden:</h5>
                    </section>
                    <section class="col-md-6">
                        <h5>{{$user->firstname}} {{$user->lastname}}</h5>
                        <h5>{{date('d-m-Y',strtotime($user->dateOfBirth))}}</h5>
                        <h5>{{$user->studentNumber}}</h5>
                        <h5>{{$user->email}}</h5>
                        <h5><span>{{$countFriends}}</span></h5>
                    </section>
                </section>
            </section>
        </section>
        <section id="settingsSection" class="offset-2 col-md-8 hidden">
            <section  class="paper-card">
                Hallo
            </section>
        </section>
    </section>
@endsection

<script>
    function transformInDataSection() {
        const dataSection = document.querySelector('#DataSection');
        const settingsSection = document.querySelector('#settingsSection');

        if(dataSection.classList.contains('hidden')) {
            dataSection.classList.remove('hidden');
            settingsSection.classList.add('hidden');
        }
    }

    function transformInSettingsSection() {
        const dataSection = document.querySelector('#DataSection');
        const settingsSection = document.querySelector('#settingsSection');

        if(settingsSection.classList.contains('hidden')) {
            settingsSection.classList.remove('hidden');
            dataSection.classList.add('hidden');
        }
    }
</script>