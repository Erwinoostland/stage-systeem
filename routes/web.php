<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

Auth::routes();


Route::middleware('auth')->group(function () {

	// Group for homework
	Route::prefix('homework')->group(function () {
    	Route::get('/', 'HomeworkController@index');
    	Route::post('/add', 'HomeworkController@add');
    	Route::post('/edit/{id}', 'HomeworkController@edit');
    	Route::get('/delete/{id}', 'HomeworkController@delete');
    	Route::post('/check/{id}', 'HomeworkController@check');
	});

	// Group for Grades
	Route::prefix('grades')->group(function () {
        Route::get('/', 'GradeController@index');
        Route::post('/add', 'GradeController@add');
        Route::post('/edit/{id}', 'GradeController@edit');
        Route::get('/delete/{id}', 'GradeController@delete');

	});

	// Group for Files
	Route::prefix('file')->group(function () {
        Route::get('/', 'FileController@index');
        Route::post('/add', 'FileController@add');
        Route::post('/edit/{id}', 'FileController@edit');
        Route::get('/delete/{id}', 'FileController@delete');
	});

	// Group for Calendar
	Route::prefix('calendar')->group(function () {
        Route::get('/', 'CalendarController@index');
	});

	// Group for Search
	Route::prefix('search')->group(function () {
		Route::get('/', 'SearchController@search');
		Route::post('/sendrequest/{id}', 'SearchController@sendRequest');
	});

	// Group for Friends
	Route::prefix('friends')->group(function () {
		Route::get('/', 'FriendController@index');
		Route::get('/delete/{id}', 'FriendController@delete');
		Route::post('/accept/{id}', 'FriendController@accept');
	});

	// Group for Achievments
	Route::prefix('achievments')->group(function () {
		Route::get('/', 'AchievmentController@index');
	});

	//Group for Profile
    Route::prefix('profile')->group(function() {
        Route::get('/', 'ProfileController@index');
    });

	// Group for Lessons
	Route::prefix('lessons')->group(function () {
        Route::get('/', 'LessonController@index');
        Route::post('/add', 'LessonController@add');
        Route::post('/edit/{id}', 'LessonController@edit');
        Route::get('/delete/{id}', 'LessonController@delete');
	});
});