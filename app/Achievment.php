<?php

namespace App;
use App\Friend;
use App\File;
use App\Homework;
use App\Grade;

use Illuminate\Database\Eloquent\Model;

class Achievment extends Model
{
    public function achievmentProgressions() {
    	return $this->hasMany('App\AchievmentProgression', 'achievmentId', 'id');
    }
} 
