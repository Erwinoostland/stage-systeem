<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievmentProgression extends Model
{
    public function user() {
    	return $this->belongsTo('App\User', 'id', 'userId');
    }

    public function achievment() {
    	return $this->belongsTo('App\Achievment', 'achievmentId', 'id');
    }
}
