<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Homework extends Model
{
    public function user() {
    	return $this->belongsTo('App\User', 'id', 'userId');
    }

    public function lesson() {
    	return $this->belongsTo('App\Lesson', 'lessonId', 'id');
    }

    public static function addHomework($date, $lessonId, $description) {
    	$newHomework = new Homework;

    	$newHomework->userId = Auth::id();
    	$newHomework->dueDate = $date;
    	$newHomework->lessonId = $lessonId;
    	$newHomework->description = $description;

    	$newHomework->save();
    }

    public static function editHomework($homeworkId, $date, $lessonId, $description) {
    	$editHomework = Homework::find($homeworkId);
    	$editHomework->dueDate = $date;
    	$editHomework->lessonId = $lessonId;
    	$editHomework->description = $description;

    	$editHomework->update();
    }
}
