<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    public function user() {
    	return $this->belongsTo('App\User', 'id', 'userId');
    }

    public function lesson() {
    	return $this->belongsTo('App\Lesson', 'id', 'lessonId');
    }

    public static function addGrade($lessonId, $description, $grade) {
        $newGrade = new Grade;

        $newGrade->userId = Auth::id();
        $newGrade->lessonId = $lessonId;
        $newGrade->description = $description;
        $newGrade->grade = $grade;

        $newGrade->save();
    }


    public static function editGrade($gradeId, $lessonId, $description, $grade) {
        $editGrade = Grade::find($gradeId);
        $editGrade->lessonId = $lessonId;
        $editGrade->description = $description;
        $editGrade->grade = $grade;

        $editGrade->update();
    }

}
