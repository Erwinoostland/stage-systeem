<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Lesson extends Model
{
    public function user() {
    	return $this->belongsTo('App\User', 'id', 'userId');
    }

    public function homeworks() {
    	return $this->hasMany('App\Homework', 'lessonId', 'id');
    }

    public static function addLesson($name) {
        $newLesson = new Lesson();

        $newLesson->name = $name;
        $newLesson->userId = Auth::id();

        $newLesson->save();
    }

    public static function editLesson($lessonId, $name){
        $editLesson = Lesson::find($lessonId);

        $editLesson->name = $name;

        $editLesson->update();

    }
}
