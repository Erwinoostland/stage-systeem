<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Lesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Achievment;
use App\AchievmentProgression;

class GradeController extends Controller
{

    public function index()
    {
        $lessons = Lesson::where('userId', Auth::id())->get();
        $grades = Grade::where('userId', Auth::id())->get();
        return view('grade/index', compact('lessons', 'grades'));
    }


    public function add(Request $request)
    {
        Grade::addGrade($request->input('addLesson'), $request->input('addDescription'), $request->input('addGrade'));

        // Check for achievments
        $getGrades = Grade::where('userId', \Auth::id())->count();
        if ($getGrades == 5) {
            // Correct(Cijfers => id(9))
            $newAchievmentProgression = new AchievmentProgression;
            $newAchievmentProgression->userId = \Auth::id();
            $newAchievmentProgression->achievmentId = 9;
            $newAchievmentProgression->finished = 1;
            $newAchievmentProgression->save();
            \Session::flash('achievment', 'Je hebt een nieuwe achievment behaald!');
        } elseif ($getGrades == 20) {
            // Correct(Geleerde => id(10))
            $newAchievmentProgression = new AchievmentProgression;
            $newAchievmentProgression->userId = \Auth::id();
            $newAchievmentProgression->achievmentId = 10;
            $newAchievmentProgression->finished = 1;
            $newAchievmentProgression->save();
            \Session::flash('achievment', 'Je hebt een nieuwe achievment behaald!');
        }

        Session::flash('success', 'Cijfer is toegevoegd!');
        return back();
    }


    public function edit(Request $request, $gradeId)
    {
        if ($request->input('editGrade') > 10 || $request->input('editGrade') < 1){
            Session::flash('danger', 'Cijfer moet tussen de 1 en een 10 zijn..');
            return back();
        } else {
            Grade::editGrade($gradeId, $request->input('editGradeLesson'), $request->input('editGradeDescription'), $request->input('editGrade'));

            Session::flash('success', 'Cijfer is gewijzigd!');
            return back();
        }
    }

    public function update(Request $request, Grade $grade)
    {
        //
    }

    public function delete($gradeId)
    {
        $deleteGrade = Grade::find($gradeId);
        $deleteGrade->delete();

        Session::flash('danger', 'Cijfer is verwijderd!');
        return back();
    }
}
