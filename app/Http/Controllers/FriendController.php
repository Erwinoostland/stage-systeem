<?php

namespace App\Http\Controllers;

use App\Friend;
use Illuminate\Http\Request;
use Auth;
use App\AchievmentProgression;
use App\Achievment;

class FriendController extends Controller
{
    public function index() {

        //Friends
        $friends = Friend::where('userId', Auth::id())->orWhere('friendId', Auth::id())->get();

        //Count Friends
        $countFriends = Friend::where('userId', Auth::id())->orWhere('friendId', Auth::id())->where('accepted', 1)->count();

        // Outgoing Requests
        $outgoingRequests = Friend::where('userId', Auth::id())->where('accepted', 0)->get();

        // Open Requests
        $openRequests = Friend::where('friendId', Auth::id())->where('accepted', 0)->get();

        return view('friends/index', compact('friends', 'countFriends', 'outgoingRequests', 'openRequests'));
    }

    public function accept($requestId) {    
        $acceptRequest = Friend::find($requestId);
        $acceptRequest->accepted = 1;
        $acceptRequest->update();

        // If this is users first friend, get achievment
        $friends = Friend::where('userId', \Auth::id())->orWhere('friendId', \Auth::id())->where('accepted', 1)->count();
        if ($friends == 1) {
            // Correct (Sociale Start => id(1))
            $newAchievmentProgression = new AchievmentProgression;
            $newAchievmentProgression->userId = \Auth::id();
            $newAchievmentProgression->achievmentId = 1;
            $newAchievmentProgression->finished = 1;
            $newAchievmentProgression-save();

            // Message
            \Session::flash('achievment', 'Je hebt een nieuwe achievment behaald!');

        } elseif ($friends == 5) {
            // Correct (Gewild => id(2))
            $newAchievmentProgression = new AchievmentProgression;
            $newAchievmentProgression->userId = \Auth::id();
            $newAchievmentProgression->achievmentId = 2;
            $newAchievmentProgression->finished = 1;
            $newAchievmentProgression-save();

            // Message
            \Session::flash('achievment', 'Je hebt een nieuwe achievment behaald!');
        }

        \Session::flash('success', 'Vriendschapverzoek is geaccepteerd.');
        return back();
    }

    public function delete($requestId) {
        $deleteFriend = Friend::find($requestId);
        $deleteFriend->delete();

        \Session::flash('danger', 'Verwijderen is gelukt!');
        return back();
    }
}
