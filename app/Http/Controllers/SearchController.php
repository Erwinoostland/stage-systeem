<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Friend;

class SearchController extends Controller
{
    public function search(Request $request) {
    	$searchPerson = $request->input('searchInput');
    	$searchResults = User::where('firstname', 'like', '%'.$searchPerson.'%')->orWhere('lastname', 'like', '%'.$searchPerson.'%')->get();
    	
    	return view('search/index', compact('searchResults'));
    }

    public function sendRequest($id) {
    	Friend::sendRequest($id);
    	\Session::flash('success', 'Vriendschapverzoek is verstuurd.');

    	return back();
    }
}
