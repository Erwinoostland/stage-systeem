<?php

namespace App\Http\Controllers;

use App\Achievment;
use App\AchievmentProgression;
use Illuminate\Http\Request;

class AchievmentController extends Controller
{
    
    public function index() {
        $allAchievments = Achievment::all();
        $allAchievmentProgressions = AchievmentProgression::where('userId', \Auth::id())->where('finished', 1)->with('achievment')->get();
        return view('achievments/index', compact('allAchievments', 'allAchievmentProgressions'));
    }
}
