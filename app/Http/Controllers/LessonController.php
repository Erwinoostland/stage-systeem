<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Grade;
use App\Homework;
use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LessonController extends Controller
{
    public function index()
    {

        // Get all lessons names from database
        $lessons = Lesson::where('userId', Auth::id())->get();

        return view('lesson/index', compact('lessons'));
    }

    public function add(Request $request)
    {
        Lesson::addLesson($request->input('lessonName'));
        \Session::flash('success','Vak is toegevoegd!');

        return back();
    }

    public function edit(Request $request, $lessonId)
    {
        Lesson::editLesson($lessonId, $request->input('editLessonName'));
        \Session::flash('success', 'Vak is gewijzigd!');
        return back();
    }

    public function delete($lessonId)
    {
         $lessonId = Lesson::find($lessonId);

         // Grades
         $delGrades = Grade::where('lessonId', $lessonId->id)->where('userId', \Auth::id())->get();
         foreach ($delGrades as $delGrade) {
             $delGrade->delete();
         }

         // Homework
         $delHomeworks = Homework::where('lessonId', $lessonId->id)->where('userId', \Auth::id())->get();
         foreach ($delHomeworks as $delHomework) {
             $delHomework->delete();
         }

         // Files
         $delFiles = File::where('lessonId', $lessonId->id)->where('userId', \Auth::id())->get();
         foreach ($delFiles as $delFile) {
            unlink(public_path('files/' . \Auth::id() . '-' . \Auth::user()->firstname . '-'. \Auth::user()->lastname.'/' . $lessonId->id .'-' . $lessonId->name .'/'.$delFile->filePath));
            $delFile->delete();
         }

         $lessonId->delete();

        \Session::flash('danger','Vak is verwijderd!');
        return back();
    }
}
