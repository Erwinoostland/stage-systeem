<?php

namespace App\Http\Controllers;

use App\AchievmentProgression;
use Illuminate\Http\Request;

class AchievmentProgressionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AchievmentProgression  $achievmentProgression
     * @return \Illuminate\Http\Response
     */
    public function show(AchievmentProgression $achievmentProgression)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AchievmentProgression  $achievmentProgression
     * @return \Illuminate\Http\Response
     */
    public function edit(AchievmentProgression $achievmentProgression)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AchievmentProgression  $achievmentProgression
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AchievmentProgression $achievmentProgression)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AchievmentProgression  $achievmentProgression
     * @return \Illuminate\Http\Response
     */
    public function destroy(AchievmentProgression $achievmentProgression)
    {
        //
    }
}
