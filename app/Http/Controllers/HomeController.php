<?php

namespace App\Http\Controllers;

use App\Achievment;
use Illuminate\Http\Request;
use App\Friend;
use App\Homework;
use App\Grade;
use App\File;
use App\Lesson;
use App\AchievmentProgression;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = \Auth::id();
        $friends = Friend::where('userId', $userId)->orWhere('friendId', $userId)->where('accepted', 1)->get();
        $friendIds =[];
        foreach($friends as $friend) {
            if($friend->userId == $userId) {
                array_push($friendIds, $friend->friendId);
            } else {
                array_push($friendIds, $friend->userId);
            }
        }

        $latestGrades = Grade::whereIn('userId', $friendIds)->orderBy('created_at', 'DESC')->limit(5)->get();
        $latestLessons = Lesson::whereIn('userId', $friendIds)->orderBy('created_at', 'DESC')->limit(5)->get();
        $latestAchievments = AchievmentProgression::whereIn('userId', $friendIds)->where('finished', 1)->orderBy('created_at', 'DESC')->limit(5)->get();
        $latestHomeworks = Homework::whereIn('userId', $friendIds)->where('done', 1)->orderBy('created_at', 'DESC')->limit(5)->get();

        return view('dashboard/index', compact('latestGrades', 'latestLessons', 'latestAchievments', 'latestHomeworks'));
    }
}
