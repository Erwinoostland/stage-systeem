<?php

namespace App\Http\Controllers;

use App\Friend;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {

        $countFriends = Friend::where('userId', Auth::id())->orwhere('friendId', Auth::id())->where('accepted', 1)->count();

        $user = User::find(\Auth::id());
        return view('profile/index', compact('user', 'countFriends'));
    }
}
