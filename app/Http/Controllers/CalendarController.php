<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function index(){

        $thisWeek = date('W', strtotime(Carbon::now()));
        $nextWeek = date('W', strtotime(Carbon::now()->addDays(7)));
        $firstDayOfWeek = date('d-m-Y', strtotime(Carbon::now()->setISODate(date('Y', strtotime(Carbon::now())), $thisWeek)));
        $firstDayOfNextWeek = date('d-m-Y', strtotime(Carbon::now()->setISODate(date('Y', strtotime(Carbon::now())), $nextWeek)));

        return view('calendar/index', compact('thisWeek', 'nextWeek', 'firstDayOfWeek', 'firstDayOfNextWeek'));
    }
}
