<?php

namespace App\Http\Controllers;

use App\Homework;
use Illuminate\Http\Request;
use Auth;
use App\Lesson;
use App\Achievment;
use App\AchievmentProgression;

class HomeworkController extends Controller
{
    public function index() {
        // Get all lessons with userId from logged in user
        $lessons = Lesson::where('userId', Auth::id())->get();
        $homeworks = Homework::where('userId', Auth::id())->orderBy('dueDate', 'ASC')->get();

        return view('homework/index', compact('lessons', 'homeworks'));
    }

    public function add(Request $request) {
        Homework::addHomework($request->input('homeworkDate'), $request->input('homeworkLesson'), $request->input('homeworkDescription'));

        // Check for achievments
        $getHomework = Homework::where('userId', \Auth::id())->count();
        if ($getHomework == 10) {
            // Correct(Huiswerk => id(6))
            $newAchievmentProgression = new AchievmentProgression;
            $newAchievmentProgression->userId = \Auth::id();
            $newAchievmentProgression->achievmentId = 6;
            $newAchievmentProgression->finished = 1;
            $newAchievmentProgression->save();
            \Session::flash('achievment', 'Je hebt een nieuwe achievment behaald!');

        } elseif ($getHomework == 20) {
            // Correct(Nerd =>id(7))
            $newAchievmentProgression = new AchievmentProgression;
            $newAchievmentProgression->userId = \Auth::id();
            $newAchievmentProgression->achievmentId = 7;
            $newAchievmentProgression->finished = 1;
            $newAchievmentProgression->save();
            \Session::flash('achievment', 'Je hebt een nieuwe achievment behaald!');
        }

        \Session::flash('success', 'Huiswerk is toegevoegd!'); 
        return back();
    }

    public function edit(Request $request, $homeworkId) {
         Homework::editHomework($homeworkId, $request->input('editHomeworkDate'), $request->input('editHomeworkLesson'), $request->input('editHomeworkDescription'));
         \Session::flash('success', 'Huiswerk is gewijzigd!'); 
        return back();
    }

    public function delete($homeworkId) {
        $deleteHomework = Homework::find($homeworkId);
        $deleteHomework->delete();

        \Session::flash('danger', 'Huiswerk is verwijderd!'); 
        return back();
    }

    public function check($homeworkId) {
        Homework::where('id', $homeworkId)->update(['done' => 1]);
        \Session::flash('success', 'Huiswerk is afgerond!'); 
        return back();
    }
}
