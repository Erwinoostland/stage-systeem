<?php

namespace App\Http\Controllers;

use App\File;
use App\Lesson;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Achievment;
use App\AchievmentProgression;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lesson::where('userId', Auth::id())->get();
        $files = File::where('userId', Auth::id())->get();
        return view('file/index', compact('lessons', 'files'));
    }

    public function add(Request $request)
    {
        $newFile = new file();

        if(empty($request->input('addFileName')) || empty($request->input('fileLesson'))) {
            Session::flash('danger', 'Alle velden zijn verplicht..');

            return back();
        }
        $newFile->fileName = $request->input('addFileName');
        $newFile->userId = Auth::id();
        $newFile->lessonId = $request->input('fileLesson');


        $user = User::find(Auth::id());
        $lesson = Lesson::find($request->input('fileLesson'));

        if($request->hasFile('uploadFile')) {
            $image = $request->file('uploadFile');
            $filePath = $image->getClientOriginalName();
            $destinationPath = public_path('files/' . $user->id . '-' . $user->firstname . '-'. $user->lastname.'/' . $lesson->id .'-' . $lesson->name .'/' );

            $request->file('uploadFile')->move($destinationPath, $filePath);

            $newFile->filePath = $filePath;
        } else {
           Session::flash('danger', 'Het is verplicht om een bestand te kiezen..');

            return back();
        }

        $newFile->save();

        // Check for achievments
        $getFiles = File::where('userId', \Auth::id())->count();
        if ($getFiles == 5) {
            // Correct(Bestanden => id(3))
            $newAchievmentProgressions = new AchievmentProgression;
            $newAchievmentProgressions->userId = \Auth::id();
            $newAchievmentProgressions->achievementId = 3;
            $newAchievmentProgressions->finished = 1;
            $newAchievmentProgressions->save();
            \Session::flash('achievment', 'Je hebt een nieuwe achievment behaald!');
        } elseif ($getFiles == 20) {
            // Correct(Cloud verslaafd => id(4))
            $newAchievmentProgressions = new AchievmentProgression;
            $newAchievmentProgressions->userId = \Auth::id();
            $newAchievmentProgressions->achievementId = 3;
            $newAchievmentProgressions->finished = 1;
            $newAchievmentProgressions->save();
            \Session::flash('achievment', 'Je hebt een nieuwe achievment behaald!');            
        }

        Session::flash('success', 'Bestand is toegevoegd');

        return back();
    }

    public function edit(Request $request, $fileId)
    {

        $editFileName = File::find($fileId);

        $editFileName->fileName = $request->input('editFileName');

        $editFileName->update();

        Session::flash('success', 'Het bestand is gewijzigd!');

        return back();
    }


    public function delete($fileId)
    {
        $deleteFile = File::find($fileId);
        $user = User::find(Auth::id());
        $lesson = Lesson::find($deleteFile->lessonId);

        if(!empty($deleteFile->filePath)){
            unlink(public_path('files/' . $user->id . '-' . $user->firstname . '-'. $user->lastname.'/' . $lesson->id .'-' . $lesson->name .'/' .$deleteFile->filePath));
        } else {
            Session::flash('danger', 'Er is geen bestand die verwijderd kan worden..');

            return back();
        }
        $deleteFile->delete();
        Session::flash('danger', 'Het bestand is verwijderd!');

        return back();
    }
}
