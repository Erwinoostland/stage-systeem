<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function homeworks()
    {
        return $this->hasMany('App\Homework', 'userId', 'id');
    }

    public function grades()
    {
        return $this->hasMany('App\Grade', 'userId', 'id');
    }

    public function friends()
    {
        return $this->hasMany('App\Friend', 'userId', 'id');
    }

    public function files()
    {
        return $this->hasMany('App\File', 'userId', 'id');
    }

    public function lessons()
    {
        return $this->hasMany('App\Lesson', 'userId', 'id');
    }

    public function role()
    {
        return $this->hasOne('App\Role', 'id', 'roleId');
    }

    public function achievmentProgressions()
    {
        return $this->hasMany('App\AchievmentProgression');
    }

    public function checkRelationship()
    {
        $friendships = Friend::where('userId', \Auth::id())->orWhere('friendId', \Auth::id())->get();

        foreach ($friendships as $friendship) {
            if ($friendship->userId == $this->id || $friendship->friendId == $this->id) {
                if ($friendship->accepted == 0) {
                    echo "Vriendschapverzoek nog niet geaccepteerd..";
                } elseif ($friendship->accepted == 1) {
                    echo "Bevriend vanaf " . date('d-m-Y', strtotime($friendship->created_at));
                }
            }
        }
    }


    public function checkFriends()
    {

    }

    public function friended()
    {
        $friendships = Friend::where('userId', \Auth::id())->orWhere('friendId', \Auth::id())->get();

        $friendRequest = 0;
        foreach ($friendships as $friendship) {
            if ($friendship->userId == $this->id || $friendship->friendId == $this->id) {
                if ($friendship->accepted == 0) {
                    $friendRequest = 1;
                    return false;
                } elseif ($friendship->accepted == 1) {
                    $friendRequest = 1;
                    return false;
                }
            }
        }
        if ($friendRequest == 0) {
            return true;
        }
    }
}

