<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    public function user() {
    	return $this->belongsTo('App\User', 'userId', 'id');
    }

    public function friend() {
    	return $this->hasOne('App\User', 'id', 'friendId');
    }

    public static function sendRequest($id) {
    	$sendRequest = new Friend;
    	$sendRequest->userId = \Auth::id();
    	$sendRequest->friendId = $id;

    	if(!$sendRequest->save()) {
    		\Session::flash('error', 'Er is iets mis gegaan!');
    		return redirect('search');
    	}
    }
}
